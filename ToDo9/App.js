import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image } from 'react-native';
//import ImageComponent from './components/imageComponent';

export default function App() {
  return (
    <View style={styles.container}>
      <Image 
      style = {styles.logo2}
      source = {{uri: 'https://i.picsum.photos/id/1084/100/100.jpg?hmac=AovAMT1ykJrT4j7RPg-nX3vMHEiMfCSHW-yv9O3AI5M'}}
      />

      <Image
      style = {styles.logo1}
      source = {require('./assets/react-logo.png')}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
      flex:1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center'
  },
  logo1:{
      width: 100,
      height: 100,
      resizeMode: 'stretch'
  },
  logo2: {
      width: 100,
      height: 100,
      resizeMode: 'stretch'
  }
});