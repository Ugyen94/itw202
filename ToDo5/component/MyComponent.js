import React from "react";
import { View, StyleSheet, Text } from "react-native";

const MyComponent = () => {
    const name = "Ugyen Kezang";

    return(
        <View style = {styles.container}> 
            <Text style = {styles.textStyle1}>
                Getting Started with React Native!
            </Text>
            <Text style = {styles.textStyle2}>
                My name is {name}
            </Text>
        </View>
    )
};
const styles = StyleSheet.create({
    textStyle1: {
        fontSize: 45,
        textAlign: "center",
    },
    textStyle2: {
        fontSize: 20,
        textAlign: "center",
    }
});
export default MyComponent;
