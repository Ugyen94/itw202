import React from "react";
import { View, StyleSheet } from 'react-native';
import Button from "../components/Buttons";
import Header from "../components/Header";
import Paragraph from "../components/Paragraph";
import Background from "../components/Background";
import Logo from "../components/Logo";
//import { NavigationContainer } from "@react-navigation/native";
//import {createStackNavigator } from "@react-navigation/stack";

export default function StartScreen({navigation}){
    return(
        <Background>
            <Logo/>
            <Header> Login Template </Header>
            <Paragraph> 
                The easiest way to start with your amazing application. 
            </Paragraph>
            <Button 
                mode="outlined"
                onPress = {() => {
                    navigation.navigate("LoginScreen")
                }}
            > Login 
            </Button>
            <Button 
                mode="contained"
                onPress = {() => {
                    navigation.navigate("RegisterScreen")
                }}
            > Sign Up 
            </Button>
        </Background>
    )
}
