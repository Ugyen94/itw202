import React from "react";
import { Text, View, StyleSheet } from 'react-native';

export default function TextComponent() {
    return(
        <View style={styles.container}>
            <Text style={styles.t1}> The <b>quick brown fox</b> jumps over the lazy dog </Text>
        </View>
    );
}
const styles= StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center', 
    },
    t1: {
        fontSize: 16,
    }
});