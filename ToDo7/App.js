import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import ViewComponent from './components/viewComponents';

export default function App() {
  return (
    <View style={{flex:1}}>
      <ViewComponent></ViewComponent>
    </View>
  );
}

