import React from "react";
import { View, StyleSheet } from 'react-native';

export default function ViewComponent() {
    return(
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <View style={{ backgroundColor: 'red', height: 100, width: 100 }}></View>
            <View style={{ backgroundColor: 'blue', height: 100, width: 100 }}></View>
        </View>
    );
}
