
import React, { useState } from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';

export default function App() {
  const [name, setName] = useState('');
    return(
        <View style = {styles.container}>
            <Text> What's your name </Text>
            <Text> Hi {name} </Text>

            <TextInput 
             secureTextEntry={true}
             onChangeText={(val) => setName(val)}
            />
        </View>
  );
}
const styles= StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    }
});